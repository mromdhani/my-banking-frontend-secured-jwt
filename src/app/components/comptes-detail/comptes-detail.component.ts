import { Component, OnInit } from '@angular/core';
import { Compte } from 'src/app/domain/compte';
import { Router, ActivatedRoute } from '@angular/router';
import { ComptesService } from 'src/app/services/comptes.service';

@Component({
  selector: 'app-comptes-detail',
  templateUrl: './comptes-detail.component.html',
  styleUrls: ['./comptes-detail.component.css']
})
export class ComptesDetailComponent implements OnInit {

  numCompte: string;
   compte: Compte =  { 'numero': null, 'proprietaire': null, 'solde': 0 };

constructor( private _router: Router,
             private _route: ActivatedRoute,
             private _service: ComptesService) { }

ngOnInit() {
  this._route.params.subscribe(
    parametres => {
            this.numCompte = parametres['id'];
            this.getCompteById(this.numCompte);
   },
    error => console.log('ATTENTION - Il y a erreur lors de la navigation vers Delete. Détails ' + error)
  );
 }
getCompteById(id) {
 this._service.getCompteById(this.numCompte).subscribe(
   resp => this.compte = resp
 );
}

 navigateToList() {
  this._router.navigate(['/list']);   // Navigation programmatique
}
}
